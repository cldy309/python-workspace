class Person(object):
	def __init__(self , name , age):
		self.name = name
		self.age = age
		print '(Initialize Person info:{0})'.format(self.name) ,
	def sayHi(self):
		''' tell person info
		'''
		print 'Hello,I am {0} and {1} years old'.format(self.name , self.age),


class Teacher(Person):
	"""docstring for Teacher"""
	def __init__(self, name , age , salary):
		super(Teacher,self).__init__(name , age)
		self.salary = salary

	def sayHi(self):
		super(Teacher , self).sayHi()
		print 'salary is {0}'.format(self.salary)
		
t = Teacher('Li' , 30 , 200000)

print

members = [t , Teacher('Wang' , 20 , 30000)]

for member in members :
	member.sayHi()
