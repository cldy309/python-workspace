# -*- coding: utf-8 -*-

import sys

__version__ = '0.1'

def sayHi(name) :
	print'Hi users' , name , 'version' , __version__

if __name__ == '__main__':

	print'The command line arguments are:'

	for i in sys.argv:
		print i

	print '\n\nThe PYTHONPATH is' , sys.path, '\n'

	number = 23

	running = True

	while running:
		guess = int(input("Enter an integer:"))

		if guess == number :
			print("Congratulation , you guess it.")
			running = False

		elif guess < number:
			print("No , it is a little higher.")

		else :
			print("No , it is a little lower.")

	else :
		print("the while loop is over.")

	print("done")


	for i in range(0 , 5):
		print(i)

	else :
		print("The for loop is over")

	def test():
		global number

		print("number is" , number)

		number = 10

		print'Changed global number to' , number

		'''这是个什么鬼
	很强大的样子
		'''
		print'''
		this is none local
		'''

		x = 5

		print "x is " , x 

		def inner_test():
			global x
			x = 10

		inner_test()

		print("Changed local x to",x)

	test()
	print('Value of number is' , number)

else:
	print'module Being imported from other'
