import time
from datetime import datetime
from flask import render_template,request,flash,redirect , url_for,current_app,make_response,abort
from .forms import NameForm , RoleForm , EditProfileForm , EditProfileAdminForm , PostForm,CommentForm
from .. import db 
from ..models import User , Role , Permission , Post , Comment
from .. import login_manager
from flask_login import login_required , current_user
from ..decorators import permission_required , admin_required


current_milli_time = lambda : int(round(time.time() * 1000))

def get_current_ts():
	now = datetime.now()
	ts = time.mktime(now.timetuple())
	return ts

from flask import Blueprint

main = Blueprint('main' , __name__)
@main.app_context_processor
def inject_permissions():
	return dict(Permission=Permission)

@main.route('/all-posts')
@login_required
def show_all():
	resp = make_response(redirect(url_for('main.welcome')))
	resp.set_cookie('show_followed' , '' , max_age=60*60)
	return resp

@main.route('/followed-posts')
@login_required
def show_followed():
	resp = make_response(redirect(url_for('main.welcome')))
	resp.set_cookie('show_followed','1' , max_age=60*60)
	return resp

@main.route('/' , methods =['GET' , 'POST'])
def welcome():
	user_agent = request.headers.get('User-Agent')
	users = None
	if current_user.is_authenticated and current_user.is_administrator():
		users = User.query.all()

	form = PostForm()
	if current_user.can(Permission.WRITE_ARTICLES) and form.validate_on_submit():
		post = Post(body = form.body.data , author=current_user._get_current_object())
		db.session.add(post)
		return redirect(url_for('main.welcome'))
	show_followed = False
	if current_user.is_authenticated:
		show_followed = bool(request.cookies.get('show_followed' , ''))

	if show_followed:
		query = current_user.followed_posts
	else:
		query = Post.query

	page = request.args.get('page', 1 ,type=int)
	pagination = query.order_by(Post.timestamp.desc()).paginate(page ,
		per_page=current_app.config['FLASKY_DEV_POSTS_PER_PAGE'] , 
		error_out=False)
	posts = pagination.items
	return render_template('welcome.html' , 
		user_agent = user_agent , 
		users = users , 
		current_time = datetime.utcnow(),
		post_form = form , 
		posts = posts ,
		pagination = pagination)

@main.route('/user/<name>')
def user(name):
	user = User.query.filter_by(username = name).first()
	if user is None:
		abort(404)
	page = request.args.get('page', 1 ,type=int)
	pagination = user.posts.order_by(Post.timestamp.desc()).paginate(page ,
		per_page=current_app.config['FLASKY_DEV_POSTS_PER_PAGE'] , 
		error_out=False)
	posts = pagination.items

	return render_template('user.html',user=user , posts = posts , pagination=pagination)

@main.route('/post/<int:id>' , methods=['GET' , 'POST'])
def post(id):
	post = Post.query.get_or_404(id)
	form = CommentForm()
	if form.validate_on_submit():
		comment = Comment(body = form.body.data , post = post , author=current_user._get_current_object())
		db.session.add(comment)
		flash('Your comment has been published.')
		return redirect(url_for('main.post' , id = post.id , page=-1))
	page = request.args.get('page',1,type=int)
	if page == -1 or page == 0:
		page = (post.comments.count() -1)/(current_app.config['FLASKY_DEV_COMMENTS_PER_PAGE']) + 1
		print post.comments.count(),page
		if page == -1 or page == 0:
			page = 1

	pagination = post.comments.order_by(Comment.timestamp.asc()).paginate(page,
		per_page = current_app.config['FLASKY_DEV_COMMENTS_PER_PAGE'],
		error_out = False)

	comments = pagination.items

	return render_template('post.html' , comments = comments , form=form , posts = [post] , pagination = pagination)

@main.route('/moderate')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate():
	page = request.args.get('page' , 1 , type=int)
	pagination = Comment.query.order_by(Comment.timestamp.desc()).paginate(page , 
		per_page=current_app.config['FLASKY_DEV_COMMENTS_PER_PAGE'],
		error_out=False)
	comments = pagination.items
	return render_template('moderate.html' , comments=comments,pagination=pagination,page = page)

@main.route('/moderate/enable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_enable(id):
	comment = Comment.query.get_or_404(id)
	comment.disabled = False
	db.session.add(comment)
	return redirect(url_for('main.moderate' , page=request.args.get('page',1,type=int)))

@main.route('/moderate/disable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_disable(id):
	comment = Comment.query.get_or_404(id)
	comment.disabled = True
	db.session.add(comment)
	return redirect(url_for('main.moderate' , page=request.args.get('page',1,type=int)))

@main.route('/editpost/<int:id>' , methods=['GET','POST'])
def editpost(id):
	post = Post.query.get_or_404(id)
	if current_user != post.author and not current_user.is_administrator():
		abort(403)

	form = PostForm()
	if form.validate_on_submit():
		post.body = form.body.data
		db.session.add(post)
		flash('The post has been updated.')
		return redirect(url_for('main.post' , id = post.id))
	form.body.data = post.body
	return render_template('edit_post.html' , form = form)

@main.route('/follow/<name>' , methods=['GET'])
@login_required
@permission_required(Permission.FOLLOW)
def follow(name):
	user = User.query.filter_by(username = name).first()
	if user is None:
		flash('Invalid user.')
		return redirect(url_for('main.welcome'))

	if current_user.is_following(user):
		flash('You are already following %s.' %name)
		return redirect(url_for('main.user' , name = name))

	current_user.follow(user)
	flash('You are following %s.' %name)
	return redirect(url_for('main.user' , name = name))

@main.route('/unfollow/<name>' , methods=['GET'])
@login_required
def unfollow(name):
	user = User.query.filter_by(username = name).first()
	if user is None:
		flash('Invalid user.')
		return redirect(url_for('main.welcome'))

	if not current_user.is_following(user):
		flash('You haven`t followed %s.' %name)
		return redirect(url_for('main.user' , name = name))

	current_user.unfollow(user)
	flash('Unfollowing %s.' %name)
	return redirect(url_for('main.user') , name = name)

@main.route('/followers/<name>' , methods=['GET'])
@login_required
def followers(name):
	user = User.query.filter_by(username = name).first()
	if user is None:
		flash('Invalid user.')
		return redirect(url_for('main.welcome'))
	page = request.args.get('page' , 1 ,type=int)
	pagination = user.followers.paginate(page , 
		per_page=current_app.config['FLASKY_DEV_POSTS_PER_PAGE'] , 
		error_out = False)
	followers = [{'user':item.follower , 'timestamp':item.timestamp}
				for item in pagination.items]

	return render_template('followers.html' , user=user , title="Followers of",
		endpoint='main.followers',pagination=pagination , followers=followers)
@main.route('/followed/<name>' , methods=['GET'])
def followed(name):
	user = User.query.filter_by(username = name).first()
	if user is None:
		flash('Invalid user.')
		return redirect(url_for('main.welcome'))		
	page = request.args.get('page' , 1 ,type=int)
	pagination = user.followed.paginate(page , 
		per_page=current_app.config['FLASKY_DEV_POSTS_PER_PAGE'] , 
		error_out = False)

	follows = [{'user':item.followed , 'timestamp':item.timestamp}
				for item in pagination.items]

	return render_template('followers.html' , user=user , title = "Followed by",
		endpoint='main.followed' , pagination=pagination , followers = follows)

@main.route('/edit-profile' , methods = ['GET' , 'POST'])
@login_required
def edit_profile():
	form = EditProfileForm()
	if form.validate_on_submit():
		current_user.location = form.location.data
		current_user.info = form.info.data
		db.session.add(current_user)
		flash('Your profile has been updated.')
		return redirect(url_for('main.user' , name = current_user.username))

	form.location.data = current_user.location
	form.info.data = current_user.info
	return render_template('edit_profile.html' , form=form)

@login_required
@admin_required
@main.route('/edit-profile/<int:id>' , methods=['GET' , 'POST'])
def edit_profile_admin(id):
	user = User.query.get_or_404(id)
	form = EditProfileAdminForm( user = user)
	if form.validate_on_submit():
		user.email = form.email.data
		user.username = form.name.data
		user.confirmed = form.confirmed.data
		user.user_role = Role.query.get(form.role.data)
		user.location = form.location.data
		user.info = form.info.data
		db.session.add(user)
		flash('The profile has been updated.')
		return redirect(url_for('main.user',name = user.username))

	form.email.data = user.email
	form.name.data = user.username
	form.confirmed.data = user.confirmed
	form.role.data = user.role_id
	form.location.data = user.location
	form.info.data = user.info
	return render_template('edit_profile.html' , form=form)


# @main.route('/user/<name>')
# def user(name):
# 	return render_template('user.html' , name = name)
@login_required
@admin_required
@main.route('/admin',methods = ['GET'])
def admin_role():
	return redirect(url_for('main.welcome'))
	# form = RoleForm()
	# if form.validate_on_submit():
	# 	role = Role.query.filter(Role.name == form.role.data).first()
	# 	users = []
	# 	if role is None:
	# 		users = None
	# 	for user in role.users:
	# 		users.append(user.username)

	# 	return render_template('role.html',users = users)

	# return render_template('role.html' , form = RoleForm())