from flask import Blueprint
from flask import jsonify
from flask import render_template
from flask import request , url_for ,flash , redirect
from flask_login import login_required , login_user , logout_user
from flask_login import current_user
from forms import LoginForm , NameForm , ChangePasswordForm , ResetPasswordRequestForm ,ResetPasswordForm
from ..models import User , Role
from .. import db
from ..email import send_mail

auth = Blueprint('auth' , __name__)

@auth.before_app_request
def before_app_request():
	if current_user.is_authenticated:
		current_user.ping()
		if not current_user.confirmed:
			if request.endpoint is not None and request.endpoint[:5] != 'auth.' and request.endpoint != 'static':
				return redirect(url_for('auth.unconfirmed'))		

@auth.route('/confirm/<token>')
@login_required
def confirm(token):
	if current_user.confirmed:
		return redirect(url_for('main.welcome'))
	if current_user.confirm(token):
		flash('You have confirmed your account. Thanks!')
	else:
		flash('Your confirmation link is invalid!')

	return redirect(url_for('main.welcome'))

@auth.route('/unconfirmed')
def unconfirmed():
	if current_user.is_anonymous or current_user.confirmed:
		return redirect(url_for('main.welcome'))
	flash('Your account is not confirmed!')
	return render_template('auth/unconfirmed.html')

@auth.route('/reconfirm')
@login_required
def reconfirm():
	token = current_user.generate_confirmation_token()
	# print url_for('auth.confirm' , token = token , _external = True)
	send_mail(current_user.email , 'Confirm Your Account',
			'auth/email/confirm' , user=current_user , token=token)
	flash('A new confirmation email has been sent to you by email.')
	return redirect(url_for('main.welcome'))

@auth.route('/login' , methods=['GET' , 'POST'])
def login():
	login_form = LoginForm()
	if login_form.validate_on_submit():
		user = User.query.filter_by(email = login_form.email.data).first()
		if user is not None and user.verify_password(login_form.password.data):
			login_user(user , login_form.remember_me.data)
			return redirect(request.args.get('next') or url_for('main.welcome'))
		flash('Invalid username or password.')
	return render_template('auth/login.html' , form = login_form)

@auth.route('/logout' , methods=['GET'])
def logout():
	logout_user()
	flash('You have been logged out.')
	return redirect(url_for('main.welcome'))

@auth.route('/register' , methods=['GET' , 'POST'])
def register():
	form = NameForm()
	if form.validate_on_submit():
		user = User(username = form.name.data,
				password = form.pswd.data,
				email = form.email.data,
				location = form.location.data,
				info = form.info.data)
		db.session.add(user)
		db.session.commit()
		token = user.generate_confirmation_token()
		# print url_for('auth.confirm' , token=token , _external=True)
		send_mail(user.email , 'Confirm Your Account',
			'auth/email/confirm' , user=user , token=token)
		flash('A confirmation email has been sent to you by email.')
		return redirect(url_for('auth.login'))
			 
		# print type(form.name.data) , type(form.email.data) , type(form.role.data) ,type(form.info.data)
	return  render_template('auth/register.html', form = form)

	# if request.method == 'GET':
	# 	return  render_template('index.html' , user_agent= user_agent , form = NameForm() , current_time=datetime.utcnow())
	# elif request.method == 'POST':
	# 	form = request.form
	# 	return render_template('summary.html',
	# 		name = form['name'],
	# 		email = form['email'],
	# 		info = form['info'])
@auth.route('/change-password' , methods = ['GET' , 'POST'])
@login_required
def change_password():
	form = ChangePasswordForm()
	if form.validate_on_submit():
		if current_user.verify_password(form.oldpass.data):
			current_user.password = form.newpass.data
			db.session.add(current_user)
			flash('Your password has been updated!')
			return redirect(url_for('main.welcome'))
		else:
			flash('Invalid password!')
	return render_template('auth/change-password.html' , form = form)

@auth.route('/reset' , methods = ['GET' , 'POST'])
def reset_password_request():
	if not current_user.is_anonymous:
		redirect(url_for('main.welcome'))
	form = ResetPasswordRequestForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email = form.email.data).first()
		if user:
			token = user.generate_reset_token()
			send_mail(user.email , 'Reset Your Password',
				'auth/email/reset_password' , user=user , token=token)
			# print url_for('auth.reset_password' , token = token , _external=True)
			flash('An email with instructions to reset your password has been sent to you.')
		return redirect(url_for('auth.login'))

	return render_template('auth/reset-password.html',form = form)

@auth.route('/reset/<token>' , methods = ['GET' , 'POST'])
def reset_password(token):
	if not current_user.is_anonymous:
		redirect(url_for('main.welcome'))

	form = ResetPasswordForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email = form.email.data).first()
		if user is None:
			return redirect(url_for('main.welcome'))
		if user.reset_password(token , form.password.data):
			flash('Your password has been updated.')
			return redirect(url_for('auth.login'))
		else:
			return redirect(url_for('main.welcome'))

	return render_template('auth/reset-password.html' , form = form)