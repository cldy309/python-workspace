#coding=utf-8
from flask_wtf import FlaskForm
from wtforms import HiddenField, \
					StringField, \
					PasswordField,\
					SubmitField,\
					TextAreaField,\
					SelectField , \
					BooleanField

from flask_pagedown.fields import PageDownField
from wtforms import ValidationError
from wtforms.validators import *
from ..models import User,Role

class NameForm(FlaskForm):
	"""docstring for NameForm"""
	# def __init__(self, arg):
	# 	super(NameForm, self).__init__()

	# hidden_tag = HiddenField()
	email = StringField('Email',
		validators = [Required() , Length(1,64) , Email()])
	name = StringField('Username',
		validators = [DataRequired() , 
		Length(1,64)])
	# 
	# 	Regexp('^[A-Za-z][A-Za-z0-9_.]*$' , 0 , 'Usernames must have only letters,numbers,dots')
	
	# role = SelectField('Role',
	# 	choices = [('admin','Administrator'),('mod','Moderator'),('user','Normal User')])
	pswd = PasswordField('Password',
		validators = [Required() , EqualTo('pswd_again' , message='Passwords must match.')])
	pswd_again = PasswordField('Confirm Password',
		validators = [Required()])

	location = TextAreaField('Location',
		validators = [Required()])

	info = TextAreaField('Info',
		validators = [Required()],
		default = u'Description')

	submit = SubmitField('Register')

	def validate_email(self , field):
		if User.query.filter_by(email = field.data).first():
			raise ValidationError('Email already registered!')

	def validate_username(self , field):
		if User.query.filter_by(username = field.data).first():
			raise ValidationError('Username already registered!')

class PostForm(FlaskForm):
	# body = TextAreaField(u'What`s on your mind?(你当前的想法)',validators=[Required()])
	body = PageDownField(u'What`s on your mind?(你当前的想法)',validators=[Required()])
	submit = SubmitField('Submit')

class CommentForm(FlaskForm):
	body = PageDownField('',validators=[Required()])
	submit = SubmitField('Comment')

class EditProfileForm(FlaskForm):
	location = TextAreaField('Location' , validators=[Length(0,64)])
	info = TextAreaField('Info')
	submit = SubmitField('Submit')

class EditProfileAdminForm(FlaskForm):
	email = StringField('Email' , validators = [Required() , Length(1,64) , Email()])
	name = StringField('Username',
		validators = [DataRequired() , 
		Length(1,64) ])
	confirmed = BooleanField('Confirmed')
	role = SelectField('Role' , coerce = int)
	location = TextAreaField('Location',validators = [Required()])
	info = TextAreaField('Info')
	submit = SubmitField('Submit')

	def __init__(self , user , *args , **kwargs):
		super(EditProfileAdminForm , self).__init__(*args , **kwargs)
		self.role.choices = [(role.id , role.name) for role in Role.query.order_by(Role.name).all()]
		self.user = user

	def validate_email(self , field):
		if field.data != self.user.email and \
			User.query.filter_by(email = field.data).first():
			raise ValidationError('Email already registered.')

	def validate_name(self , field):
		if field.data != self.user.username and \
			User.query.filter_by(username = field.data).first():
			raise ValidationError('Username already in use.')

class RoleForm(FlaskForm):
	role = SelectField('Role',
		choices = [('admin','Administrator'),('mod','Moderator'),('user','Normal User')])

	submit = SubmitField('Get!')

class LoginForm(FlaskForm):
	email = StringField('Email' , 
		validators = [Required() , Length(1,64) , Email()])
	password = PasswordField('Password',
		validators = [Required()])
	remember_me = BooleanField('Keep me loggin in')
	submit = SubmitField('Login')

class ChangePasswordForm(FlaskForm):
	oldpass = PasswordField('Old Password',
		validators = [Required()])
	newpass = PasswordField('New Password',
		validators = [Required() , EqualTo('confirmpass' , message='Passwords must match.')])
	confirmpass = PasswordField('Confirm Password',
		validators = [Required()])

	change = SubmitField('Change')

class ResetPasswordRequestForm(FlaskForm):
	email = StringField('Email' , 
		validators = [Required() , Length(1,64) , Email()])
	submit = SubmitField('Reset')

class ResetPasswordForm(FlaskForm):
	email = StringField('Email' , 
		validators = [Required() , Length(1,64) , Email()])
	password = PasswordField('New Password',
		validators = [Required()])
	password2 = PasswordField('Confirm Password',
		validators = [Required() , EqualTo('password',message='Passwords must match.')])
	submit = SubmitField('Reset Password')

	def validate_email(self , field):
		if User.query.filter_by(email = field.data).first() is None:
			raise ValidationError('Unknown email address.')
		