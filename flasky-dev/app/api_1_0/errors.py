from flask import jsonify
from . import api
from ..exceptions import ValidationError

def bad_request(message):
	response = jsonify({'error':'bad request' , 'msg':message})
	response = 400
	return response

def forbidden(msg):
	response = jsonify({'error':'forbidden','msg':msg})
	response.status_code = 403
	return response

def unauthorized(message):
	response = jsonify({'error':'unauthorized','msg':message})
	response.status_code = 401
	return response

@api.errorhandler(ValidationError)
def validation_error(e):
	return bad_request(e.args[0])
