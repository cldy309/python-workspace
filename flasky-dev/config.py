import os 

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
	SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
	SQLALCHEMY_COMMIT_ON_TEARDOWN = True
	FLASKY_DEV_ADMIN = os.environ.get('FLASKY_DEV_ADMIN') or 'flaskydevblog@163.com'
	FLASKY_DEV_POSTS_PER_PAGE = os.environ.get('FLASKY_DEV_PER_PAGE') or 20
	FLASKY_DEV_COMMENTS_PER_PAGE = os.environ.get('FLASKY_DEV_PER_PAGE') or 10
	SQLALCHEMY_TRACK_MODIFICATIONS = True

	FLASKY_DEV_MAIL_SUBJECT_PREFIX = '[FlaskyDevBlog]'
	FLASKY_DEV_MAIL_SENDER = 'FDB Admin <flaskydevblog@163.com>'
	MAIL_SERVER = 'smtp.163.com'
	MAIL_PORT = 25
	MAIL_USE_TLS = False
	MAIL_USERNAME = os.environ.get('FLASKY_MAIL_USERNAME')
	MAIL_PASSWORD = os.environ.get('FLASKY_MAIL_PASSWORD')

	@staticmethod
	def init_app(app):
		pass

class DevConfig(Config):
	DEBUG = True
	# FLASKY_DEV_MAIL_SUBJECT_PREFIX = '[Flasky-dev]'
	# FLASKY_DEV_MAIL_SENDER = 'Flasky-dev Admin <clong@qiyi.com>'
	# MAIL_SERVER = 'smtp.googlemail.com'
	# MAIL_PORT = 587
	# MAIL_USE_TLS = True
	# MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
	# MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
	SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
		'sqlite:///' + os.path.join(basedir , 'flasky-dev.sqlite')


class TestingConfig(Config):
	TESTING = True
	SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
		'sqlite:///' + os.path.join(basedir , 'flasky-test.sqlite')


class ProductionConfig(Config):
	# SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
	# 	'sqlite:///' + os.path.join(basedir , 'user.sqlite')


	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
		'mysql://blog:blog@10.153.73.84/flasky_dev?charset=utf8'

config = {
	'development' : DevConfig,
	'testing' : TestingConfig,
	'production' : ProductionConfig,

	'default' : DevConfig
}
		
		
