#!/bin/bash
set -e

LOGFILE=log/gunicorn.log
ERRORFILE=log/error.log
ACCESSFILE=log/access.log
LOGDIR=$(dirname $LOGFILE)

cores=$(grep -c processor /proc/cpuinfo)
workers_per_core=5

NUM_WORKERS=$(echo $cores $workers_per_core | awk '{printf "%0.2f\n" , $1*$2}')
NUM_WORKERS=${NUM_WORKERS%.*}
source env/bin/activate
test -d $LOGDIR || mkdir -p $LOGDIR

exec gunicorn manage:app -b 0.0.0.0:80 -w $NUM_WORKERS -e FLASKY_CONFIG="production" \
	--log-level=info --log-file $LOGFILE --error-logfile $ERRORFILE --access-logfile $ACCESSFILE -D 2>>$ERRORFILE
