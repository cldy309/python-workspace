#-*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem
import scrapy
import os

class RepofilePipeline(object):
	
    def process_item(self, item, spider):
    	item_url = item['url']
    	re = scrapy.Request(item_url)
    	df = spider.crawler.engine.download(re , spider)
    	df.addBoth(self.download_item, item)
        return df

    def download_item(self , response , item):
    	if response.status != 200:
    		return item

    	url = item['url']
    	filename = item['name']
    	fullpath = item['path']

    	dirs = fullpath.decode().split('/')

    	path = os.path.join(*dirs[0:-1])

    	if not os.path.exists(path):
    		os.makedirs(path)

    	with open(os.path.join(path , filename) , 'wb') as f:
    		f.write(response.body)

    	return item


