#coding=utf8

import scrapy
import os
from ..items import RepofileItem

endwiths = ['pom','md5','sha1','jar','xml','war','gz','properties','zip']

class MavenSpider(scrapy.Spider):
	name = 'mavenspider'

	def start_requests(self):
		urls = ['http://maven.qiyi.virtual/a/iqiyi-internal-repo/']

		for url in urls:
			yield scrapy.Request(url=url , callback=self.parse)

	def parse(self , response):
		refselectors = response.css('a::attr(href)')
		refs = refselectors.extract()
		for ref in refs:
			if ref == u'hello/' or ref == u'../':
				continue
			url = response.urljoin(ref)
			if self.endswith(url):
				item = RepofileItem()
				item['name'] = ref
				item['url'] = url
				item['path'] = url.split('//')[-1]
				yield item
			else:
				yield scrapy.Request(url , callback=self.parse)
	
	def endswith(self,url):
		res = False
		for end in endwiths:
			if url.endswith(end):
				res = True

		return res

