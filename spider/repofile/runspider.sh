#!/bin/bash

set -e 

LOGFILE=log/maven.log
ERRORFILE=log/error.log
SCRAPEDITEMS=log/scrapeditems.json
LOGDIR=$(dirname $LOGFILE)

PID=scrapy.pid

if [ -e "$PID" ]; then
	kill -9 `cat $PID`
fi

source ../env/bin/activate
test -d $LOGDIR || mkdir -p $LOGDIR

scrapy crawl mavenspider \
	--output=$SCRAPEDITEMS --logfile=$LOGFILE 2>>$ERRORFILE &

echo $! > $PID
