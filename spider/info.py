import time
import requests
import os

basic_url = 'http://qyxy.baic.gov.cn/CheckCodeCaptcha'
local_path = './validate_pic'

current_milli_time = lambda: int(round(time.time() * 1000))



def download_code():
	if not os.path.exists(local_path):
		os.makedirs(local_path)

	for i in range(10):
		r = requests.get(url = basic_url , params={'currentTimeMillis':current_milli_time()})
		print r.url
		print r.status_code

		local_file = '%s%s%s.png' %(local_path , os.sep , i)
		with open(local_file , 'wb') as f:
			f.write(r.content)
		time.sleep(1)

if __name__ == '__main__':
	print current_milli_time()
	download_code()