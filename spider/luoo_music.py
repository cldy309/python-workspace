#coding=utf-8

import os
import requests
from bs4 import BeautifulSoup
import Queue
import threading
from time import ctime
from termcolor import colored

luoo_site = 'http://www.luoo.net/music/'
luoo_site_mp3 = 'http://luoo-mp3.kssws.ks-cdn.com/low/luoo/radio%s/%s.mp3'

def fix_char(ss):
	for c in ['/','\\\\','"']:
		ss = ss.replace(c,'-')

	return ss

class LuooSpider(threading.Thread):
	"""docstring for LuooSpider"""
	def __init__(self, url, vols , queue=None):
		super(LuooSpider, self).__init__()
		print '[luoo spider class]'
		print '=' * 20
		self.url = url
		self.queue = queue
		self.vol = 1
		self.vols = vols

	def run(self):
		for vol in self.vols:
			try:
				self.spider(vol)
			except Exception as e:
				continue

	def spider(self , vol):
		url = luoo_site + str(vol)
		try:
			# requests.get(url , proxy)
			res = requests.get(url)
			soup = BeautifulSoup(res.content , 'lxml')
			title = soup.find('span' , {'class' : 'vol-title'}).text
			cover = soup.find('img' , {'class' : 'vol-cover'})['src']
			desc = soup.find('div' , {'class' : 'vol-desc'})
			track_names = soup.find_all('a' , {'class':'trackname'})
			track_count = len(track_names)
			tracks = []
			for track in track_names:
				_id = str(int(track.text[:2])) if (int(vol) < 12) else track.text[:2]
				_name = fix_char(track.string[4:])
				# _name = track.text[4:].encode('utf-8')
				tracks.append({'id':_id , 'name':_name})

			phases = {
				'vol':vol,
				'title':title,
				'cover':cover,
				'desc':desc,
				'track_count':track_count,
				'tracks':tracks
			}
			self.queue.put(phases)
		except Exception as e:
			print colored(url + ':' + e , 'red')
			raise 
		

class LuooDownloader(threading.Thread):
	"""docstring for LuooDownloader"""
	def __init__(self, downloader_id, url , localpath , queue=None):
		super(LuooDownloader, self).__init__()
		self.downloader_id = downloader_id
		self.url = url
		self.localpath = localpath
		self.queue = queue
		self.__counter = 0

	def run(self):
		while True:
			if self.queue.qsize() <= 0:
				pass
			else:
				phase = self.queue.get()
				self.download(phase)

	def download(self , phase):
		for track in phase['tracks']:
			url = self.url % (phase['vol'] , track['id'])
			local_path = '%s%s%s' % (self.localpath ,os.sep, phase['vol'])
			if not os.path.exists(local_path):
				os.makedirs(local_path)

			local_file = '%s%s%s-%s.mp3' % (local_path ,os.sep, track['id'] , track['name'])
			if not os.path.isfile(local_file):
				print '[downloader %s]: downloading: %s ...' %(self.downloader_id,track['name'])
				# requests.get(url , proxy , headers)
				res = requests.get(url)
				with open(local_file , 'wb') as f:
					f.write(res.content)
				print colored('[downloader %s]: %s done!' %(self.downloader_id , track['name']) , 'green')
			else :
				print '[downloader %s]: skip %s !' %(self.downloader_id , track['name'])

if __name__ == '__main__':
	spider_queue = Queue.Queue()

	downloaders = []

	luoo = LuooSpider(luoo_site , vols=range(800) , queue=spider_queue)
	luoo.setDaemon(True)
	luoo.start()

	downloader_count = 5
	for i in range(downloader_count):
		download = LuooDownloader(i , luoo_site_mp3 , './mp3' , queue=spider_queue)
		downloaders.append(download)

	print 'crawling start at' , ctime()
	for download in downloaders:
		download.setDaemon(True)
		download.start()

	for download in downloaders:
		download.join()

	print 'crawling all end at' , ctime()


