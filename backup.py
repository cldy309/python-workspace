import os 
import time

source_dir = './'

source = os.listdir(source_dir)

print 'Source files: {0}'.format(source)

today = source_dir + os.sep + time.strftime('%Y%m%d')

now = time.strftime('%H%M%S')

if not os.path.exists(today):
	os.mkdir(today)
	print 'Successfully created directory',today

target = today + os.sep + now + '.zip'

zip_command = 'zip -qr {0} {1}'.format(target , ' '.join(source))

if os.system(zip_command) == 0:
	print 'Successfully backup to',target
else :
	print 'Backup failed!',target


