//#################北京首页login js  和 企业信息 工商公示页面所需js wjy 2014-01-25 #########################

//表单文本输入框默认值的自动清除和恢复~~~~begin~~~~

//1:-----获取焦点的时候，对input赋予空置
function onFocusVal(obj) {
	if(obj.value==obj.defaultValue){
		obj.value='';
		obj.style.color='#262626';
	}
}
//2:-----这个函数是当失去焦点的时候，value的值又显示在input框中
function onBlurVal(obj) {
	if(obj.value==''){
		obj.value=obj.defaultValue;
		obj.style.color='#D2D2D2';
	}else{
		obj.style.color='#262626';
	}
}              
//切换验证码
function checkCode(){
	document.getElementById('MzImgExpPwd').src = rootPath+"/"+checkCodeServletName+"?currentTimeMillis="+currentTimeMillis+"&num="+Math.ceil(Math.random()*100000);;		
}	 

//回车 查询
function fnKeydown(){
	if(event.keyCode==13){
		bjlogin();
	    return false;
	}
}

//执行登录操作
//在登录前判断input框不为空：1------
//而后判断校验码是否正确：2-------
//校验码正确之后，执行ajax提交数据：3------
function bjlogin() {
	 if($.trim($("#keyword").val()) == ''){
	    	 alert("请输入企业名称或注册号");
	         $("#keyword").val('请输入企业名称或注册号');
	         $("#keyword").focus();
	         $("#keyword").select();
	         return false; 
	    }
	    if($.trim($("#keyword").val()) == '请输入企业名称或注册号'){
	    	 alert("请输入企业名称或注册号");
	         $("#keyword").focus();
	         $("#keyword").select();
	         return false; 
	    }
		alert_win.style.display='block';
		zmdid.style.display='block';
		$("#checkcodeAlt").focus();
	
}
//点击搜索按钮
	function fn_search(){
	  ajaxCheckCode();//2-------
	}
/*
 * bjQyList
 * @return {TypeName} 
 */
function bjQyListQuery() {
	var keyword = $("#keyword").val();
	var checkcode = $("#checkcode").val();
	var kDefault = $("#keyword")[0];
	var cDefault = $("#checkcode")[0];
	if(jQuery.trim(keyword) == ""){
		alert("提示：请输入企业名称或注册号!");
		$("#keyword").val("");
		$("#keyword").focus();
		return false
	}
	if(jQuery.trim(checkcode) == "" || checkcode == cDefault.defaultValue){
		alert("提示：请输入右侧验证码!");
		$("#checkcode").val("");
		$("#checkcode").focus();
		return false
	}
	ajaxCheckCode();//2-------
}

/**
 * 返回地方局首页
 */
function toIndex(){
	window.location =  rootPath + '/gjjbj/gjjQueryCreditAction!toIndex.dhtml'; 
}
/**
 * 返回全国地图首页
 */
function toCountryIndex(){
	//window.location =  rootPath + '/gjjbj/gjjQueryCreditAction!toCountryIndex.dhtml'; 
	var url = "http://gsxt.saic.gov.cn";
	window.location = url;
}
/**
 * 返回使用说明
 */
function toMark(){
	window.location =  rootPath + '/gjjbj/gjjQueryCreditAction!toMark.dhtml'; 
}

/**
 *1:验证校验码是否正确。
 *  如果成功，则验证商家是否存在
 *  如果失败，则提示错误信息
 */
function ajaxCheckCode(){
	var url = rootPath +'/gjjbj/gjjQueryCreditAction!checkCode.dhtml'; 
	var checkcodeAlt = $("#checkcodeAlt").val();
	$("#checkcode").val(checkcodeAlt);
	var params = $('#bjLoginForm').serialize(); 
	$.ajax({
		type: "post",
		url:url,
		data:params,
		async: false,
		success: function(data) {
		  if("success"==data){
			  //此处进行限制词判断---begin~~~~
			  var literUrl = rootPath +'/gjjbj/gjjQueryCreditAction!findLiteralWord.dhtml'; 
			  $.ajax({
					type: "post",
					url: literUrl,
					data: params,
					async: false,
					success: function(data) {
					  if("success"==data){
						 $("#bjLoginForm").submit();//form表单提交，而非用window.open();  eg:%25过滤.
						 alert_win.style.display='none';
						 zmdid.style.display='none';
						 //setTimeout('checkCode()', 2000);
						 //checkCode();//信用查询》查询首页：输入查询条件点击‘搜索’按钮后，验证码未刷新，可继续更新查询条件点击搜索查询
					  }else if("fail"==data){
				    	  alert("提示：您输入的关键字不符合查询要求,请重新输入!");
				    	  $("#checkcodeAlt").val("");
				    	  alert_win.style.display='none';//输入限制词，提示框应消失
						 zmdid.style.display='none';
					  }
				    },
				    error: function(XMLHttpRequest, textStatus, errorThrown) {       
						alert('提示：读取超时，请检查网络连接');        
					}
				});
			  //此处进行限制词判断---end~~~~
		  }else if("fail"==data){ 
	    	  alert("提示：验证码输入错误!");
	    	  $("#checkcodeAlt").val("");
	    	  checkCode();
		  }else if("tryCatch"==data){
			  alert("提示：程序异常!");
		  }
	    },
	    error: function(XMLHttpRequest, textStatus, errorThrown) {       
			alert('读取超时，请检查网络连接');        
		}
	});
}


/**
 * 校验码正确的情况下，执行查询数据List
 */
function getBjQyList(){
	var url = rootPath + '/gjjbj/gjjQueryCreditAction!getBjQyList.dhtml'; 
	var params = $('#bjLoginForm').serialize(); 
	$.ajax({
		type: "POST",
		url:url,
		data:params,
		async: false,
	    error: function(request) {
	        alert("提示：程序出错！");
	    },
	    success: function(data) {
			if("tryCatch"==data){
			  	alert("提示：程序异常!");
		  	}else{
				window.location=url;
		  }
	    }
	});
}

//工商公示页面所需js
//回车查询
function fnKeydownQuery(){
	if(event.keyCode==13){
		bjQyListQuery();
	    return false;
	}
}

/**
 * 在首页列表为展示出来的数据，通过查询方法的到所需数据
 */
function queryDetal(){
	var keyword = $("#keyword").val();
	var params = $('#bjLoginForm').serialize(); 
	if(jQuery.trim(keyword) == "" ){
		alert("提示：请输入查询条件!");
		return false;
	}else{
		 //此处进行限制词判断---begin~~~~
		  var literUrl = rootPath +'/gjjbj/gjjQueryCreditAction!findLiteralWord.dhtml'; 
		  $.ajax({
				type: "post",
				url: literUrl,
				data: params,
				async: false,
				success: function(data) {
				  if("success"==data){
					 $("#bjLoginForm").submit();//此处为 bjQjList.jsp 的搜一下  //form表单提交，而非用window.open();  eg:%25过滤.
				  }else if("fail"==data){
			    	  alert("提示：您输入的关键字不符合查询要求,请重新输入!");
				  }
			    },
			    error: function(XMLHttpRequest, textStatus, errorThrown) {       
					alert('提示：读取超时，请检查网络连接');        
				}
			});
		  //此处进行限制词判断---end~~~~
		
	}
}

/**
 * 根据具体企业，查看详细情况
 */
function openEntInfo(regName,regId,regNo, credit_ticket){
	//var entName = encodeURIComponent(regName);  //此处将+'&entName='+entName 去掉，是为了防止企业名称中有() 安全过滤，直接将此过滤掉
	var url = rootPath + '/gjjbj/gjjQueryCreditAction!openEntInfo.dhtml?entId='+regId+'&credit_ticket='+credit_ticket+'&entNo='+regNo+'&timeStamp='+new Date().getTime();//给该url一个时间戳~~这样就必须每次从服务器读取数 
	window.location.href = url;
}
