//#################工商公示  企业公示 其他部门公示 js 2014-02-09 #########################

/**
 * 返回地方局首页
 */
function toIndex(){
	window.location.href =  rootPath + '/gjjbj/gjjQueryCreditAction!toIndex.dhtml'; 
}
/**
 * 返回全国地图首页
 */
function toCountryIndex(){
	//window.location =  rootPath + '/gjjbj/gjjQueryCreditAction!toCountryIndex.dhtml'; 
	window.open('http://gsxt.saic.gov.cn','_parent')
}

/**
 * 打开相对应的页面  去掉了名称的传参：+'&entName='+entNameStr
 * @param {Object} str
 */
function togo(str,credit_ticket){
	var entNameStr =encodeURIComponent(entName);
	var url;
	//工商公示
	if(str == '1'){
		url = rootPath + '/gjjbj/gjjQueryCreditAction!openEntInfo.dhtml?entId='+entId+'&entNo='+entNo+'&credit_ticket='+credit_ticket+'&str='+str+'&timeStamp='+new Date().getTime();//给该url一个时间戳~~这样就必须每次从服务器读取数
	}
	//企业公示
	if(str == '2'){
		url = rootPath + '/gjjbj/gjjQueryCreditAction!openInfo.dhtml?entId='+entId+'&entNo='+entNo+'&credit_ticket='+credit_ticket+'&str='+str+'&timeStamp='+new Date().getTime();//给该url一个时间戳~~这样就必须每次从服务器读取数
	}
	//其他公示
	if(str == '3'){
		url = rootPath + '/gjjbj/gjjQueryCreditAction!openInfo.dhtml?entId='+entId+'&entNo='+entNo+'&credit_ticket='+credit_ticket+'&str='+str+'&timeStamp='+new Date().getTime();//给该url一个时间戳~~这样就必须每次从服务器读取数
	}
	
	window.location = url;
}



/**
 * 当选中某一个页签的时候，背景颜色随之改变
 * @param {Object} divId
 * @param {Object} ele
 */
function changeStyleImage(divId){ 
        var liAry=document.getElementById(divId).getElementsByTagName("li");             
        var liLen=liAry.length;
        var liID='3';
        for(var i=0;i<liLen;i++)
        {
            if(liAry[i].id==liID)
            {
                liAry[i].className="current";
            }
            else
            {
               liAry[i].className="";
            }
        }
    }
		
//###############企业公示  begin  
function entpubinfo(methodName){
	var url =  rootPath + "/qygsbj/entPubInfoAction!"+methodName+".dhtml?timeStamp="+new Date().getTime();//给该url一个时间戳~~这样就必须每次从服务器读取数据
	$.ajax({
	   type: "post",
	   url: url,
	   data: {entId:entId},
	   success: function(msg){
		 $("#inner").html('');
	     $("#inner").html(msg); 
	   }
	});
}
//###############其他部门公示  begin  
function entotherinfo(){
 
}
 
/**
 * 打开图片
 */
 function openPic(picPath,keyValue){
	var pathStr =encodeURIComponent(jQuery.trim(picPath));
	var url = rootPath + '/qygsbj/entPubInfoAction!openPic.dhtml?picPath='+pathStr+'&keyValue='+keyValue;
	window.open(url);
}  
 
//查询 分页
function jumppage(pageNo){
 	var zzs =/^[1-9]\d*$/;　　//正整数 
 	var pagescount = $("#pagescount").val();
 	if(pageNo == '0' ||  pageNo == parseInt(pagescount) + 1){
 		//此处为公用方法的判断，如果为最前页 最后页 不执行查询操作
 	}else{
		document.getElementById("pageNos").value = pageNo;
		document.getElementById("iframeFrame").submit();
 	}
}
 /**
 * iframe高度自适应
 * @param {Object} obj
 */
function SetCwinHeight(obj) {  
  		 var ifm = obj;        
  		 /*if (document.getElementById) {     
  			 if (cwin && !window.opera) {       
  				 if (cwin.contentDocument && cwin.contentDocument.body.offsetHeight){       
  						 cwin.height = cwin.contentDocument.body.offsetHeight + 20; //FF NS  20
  						 }
  					 else if (cwin.Document && cwin.Document.body.scrollHeight){     
  						 cwin.height = cwin.Document.body.scrollHeight + 10; //IE      
  						 }    
  				 }
  			 else {            
  				 alert("else");
  				 if (cwin.contentWindow.document && cwin.contentWindow.document.body.scrollHeight)  {        
  					 cwin.height = cwin.contentWindow.document.body.scrollHeight; //Opera        
  					 }         
  			 }      
  		 }*///高度自适应勿删备份
  		 
	var subWeb = document.frames ? document.frames[obj.id].document : ifm.contentDocument; 
	if(ifm != null && subWeb != null) { 
		ifm.height = subWeb.body.scrollHeight; 
	} 
 }
	  


/**
 * 高度自适应勿删 备份
 */
function iFrameHeight() { 
	var ifm= document.getElementById("dcdyFrame"); 
	var subWeb = document.frames ? document.frames["dcdyFrame"].document : ifm.contentDocument; 
	if(ifm != null && subWeb != null) { 
	ifm.height = subWeb.body.scrollHeight; 
	} 
} 
