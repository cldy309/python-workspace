# supervisor

[supervisor](http://supervisord.org/) 是一个进程管理服务，和 Ubuntu 下的
Upstart，Fedora 下的 systemd 类似，可以负责监控进程，重启失败进程等。

Docker 默认在每个 container 中只支持启动一个进程，称为「入口进程」，
入口进程退出则 Docker 容器退出。如果业务想要在 Docker 中运行超过一个进程，
例如：需要运行 crond 进程来定期清理日志等，则需要使用 Supervisor
来管理和启动这些进程。

例如：在不使用 Supervisor 的情况下，如果 Docker 容器中运行了业务进程以及 crond
进程，那么只能有一个作为容器入口进程，而另一个入口进程异常退出后并不会导致容器退出，
所以会导致未知异常，这是我们不想要的。引入 Supervisor 后，Supervisor
作为容器的入口进程，负责管理其它进程，并且可以在被它管理的进程退出时自动重启进程。

将 supervisor 加入到自己的 Docker 镜像中非常简单，只需要在你的 Dockerfile
中添加如下一行：

```:Dockerfile
...
IMPORT docker-snippets/supervisor/Dockerfile
```

**注意：**首先需要将
[docker-snippets](http://scm.qiyi.virtual/docker/dockerfiles)
目录复制到当前项目目录。
