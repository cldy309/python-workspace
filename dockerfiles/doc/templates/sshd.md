# sshd

如果想要在你的 Docker 镜像中开启 sshd，可以使用这里的 sshd 模板。

首先，需要将你的 ssh public key 加入到
docker-snippets/sshd/resources/authorized_keys 中。

然后，使用 IMPORT 指令包含 sshd 的 Dockerfile 模板即可。

```:Dockerfile
...
IMPORT docker-snippets/supervisor/Dockerfile
IMPORT docker-snippets/sshd/Dockerfile
...
```
