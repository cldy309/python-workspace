# crond

如果想在你的 Docker 镜像中使用 cron 任务，可以使用这里的 crond 模板。

首先，将你的 cron 任务文件命名为 myjob.cron，并且放入
docker-snippets/crond/resources/ 目录，任务文件的编写可以参考其中的
example.cron。

然后，使用 IMPORT 包含 crond 模板即可，如下：

```:Dockerfile
...
IMPORT docker-snippets/supervisor/Dockerfile
IMPORT docker-snippets/crond/Dockerfile
...
```

**注意：**crond 依赖于 supervisor，所以需要首先导入 supervisor 的 Dockerfile
模板。
