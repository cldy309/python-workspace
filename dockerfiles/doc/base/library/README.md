# 本地镜像

我们在私有 Docker Registry 的 library 命名空间下提供了多种本地镜像，列表如下，如需增加项目，请 [获取技术支持](../../../README.md#获取帮助)。

* [CentOS 6](centos6.md)
* [CentOS 7](centos7.md)
* [Ubuntu](ubuntu.md)
* [QAE](qae.md)
* [Gitlab Runner](gitlab-runner.md)