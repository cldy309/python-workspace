# CentOS 7

CentOS 7.x 是目前公司内部使用较广泛的镜像，docker-registry 中最新的
centos7:7-iqiyi-7 镜像基于 CentOS 官方镜像
[centos:centos7.2.1511](https://registry.hub.docker.com/u/library/centos/)
构建。做了以下修改：

 * 设置时区为 Asia/Shanghai
 * 安装了公司内部 yum repo
 * 升级各系统组件到 2016-03-08 最新版本

## 使用方式

使用 centos7:7-iqiyi-7 作为基础镜像非常简单，如下：

```:Dockerfile
FROM docker-registry.qiyi.virtual/library/centos7:7-iqiyi-7
...
```
