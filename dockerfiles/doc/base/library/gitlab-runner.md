# GitLab Runner

GitLab Runner 用来执行 GitLab CI 任务，GitLab CI 支持 2 中 Runner：

  - Shared Runner
  - Specific Runner

Shared Runner 使用 Docker Executor，也就是会将 CI 任务运行在 Docker 容器中，
所以用户需要在 `.gitlab-ci.yml` 中指定要使用的用于执行 CI 任务的 Docker。

这些镜像必须基于这里提供的 GitLab Runner 基础镜像来构建。

所有 GitLab Runner 基础镜像都上传到了 docker-registry.qiyi.virtual/gitlab-runner
这个命名空间下，用户可以在 http://dr.qiyi.virtual 查看。

## golang

构建 docker-registry.qiyi.virtual/gitlab-runner/golang 这个镜像的 Dockerfile
可以在本项目中 [查看](../../../library/gitlab-runner/golang/1.4/Dockerfile)，主要做了以下几件事：

  - 基于 golang 官方镜像 golang:1.4.3
  - 使用 163 的 Debian 软件源
  - 安装 Docker 1.7.1
