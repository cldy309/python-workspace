# ubuntu

## ubuntu 14.04

ubuntu 14.04 是目前使用广泛的镜像，docker-registry 中的
ubuntu:14.04-iqiyi-3 镜像基于 ubuntu:14.04.3 官方镜像
[ubuntu](https://registry.hub.docker.com/u/library/ubuntu/)
构建。做了以下修改：

 * 设置时区为 Asia/Shanghai
 * 使用 163 ubuntu 镜像源

## 使用方式

使用 ubuntu:14.04-iqiyi-3 作为基础镜像非常简单，如下：

```:Dockerfile
FROM docker-registry.qiyi.virtual/library/ubuntu:14.04-iqiyi-3
...
```

## ubuntu 15.10

ubuntu 15.10 是目前使用广泛的镜像，docker-registry 中的
ubuntu:15.10-iqiyi-1 镜像基于 ubuntu:15.10 官方镜像
[ubuntu](https://registry.hub.docker.com/u/library/ubuntu/)
构建。做了以下修改：

 * 设置时区为 Asia/Shanghai
 * 使用 163 ubuntu 镜像源

## 使用方式

使用 ubuntu:15.10-iqiyi-1 作为基础镜像非常简单，如下：

```:Dockerfile
FROM docker-registry.qiyi.virtual/library/ubuntu:15.10-iqiyi-1
...
```
