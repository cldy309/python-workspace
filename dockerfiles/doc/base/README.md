# 基础镜像

Docker 镜像是一种层次镜像，或者叫做 Overlay 镜像，通过一层一层的叠加镜像形成最终可用的镜像。

而基础镜像故名思议就是处于镜像底层的镜像。实际上，每个镜像都可以作为基础镜像，使用基础镜像则通过 `FROM` 指令来实现。

在公司内部的 `docker registry` 中，我们分别在 `docker` 和 `library` 两个命名空间下提供了从官方同步的常用镜像和经过我们优化的本地镜像。

下面将介绍现有的一些通用的基础镜像的功能及其使用方法。

* [本地镜像](library/README.md)
    * [CentOS 6](library/centos6.md)
    * [CentOS 7](library/centos7.md)
    * [Ubuntu](library/ubuntu.md)
    * [QAE](library/qae.md)
    * [Gitlab Runner](library/gitlab-runner.md)
* [官方镜像](docker/README.md)
