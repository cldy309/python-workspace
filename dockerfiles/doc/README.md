# Docker 基础镜像及 Dockerfile 模板

本项目主要用于跟踪一些计算云提供的基础镜像 Dockerfile 以及一些非常有用的常见
组件 Dockerfile 模板。

基础镜像包括但不限于：

  * CentOS 6
  * CentOS 7
  * Ubuntu 14.04
  * Ubuntu 15.10
  * QAE

常见组件 Dockerfile 模板包括但不限于：

  * supervisor
  * crond
  * sshd

本项目的初衷在于提供一些 Dockerfile 模板，以便加快用户编写 Dockerfile
的速度，同时也降低用户的学习成本。
