# README

本项目旨在为公司 Docker 用户提供标准、安全、方便的 Docker 基础镜像和 Dockerfile 模板，内容主要包含以下两部分：

  * 基础镜像的 Dockerfile
  * 常见组件的 Dockerfile 模板

## 文档目录

* [基础镜像](doc/base/README.md)
    * [本地镜像](doc/base/library/README.md)
        * [CentOS 6](doc/base/library/centos6.md)
        * [CentOS 7](doc/base/library/centos7.md)
        * [Ubuntu](doc/base/library/ubuntu.md)
        * Debian
        * Golang
        * [QAE](doc/base/library/qae.md)
        * [Gitlab Runner](doc/base/library/gitlab-runner.md)
    * [官方镜像](doc/base/docker/README.md)

* [模板](doc/templates/README.md)
    * [supervisor](doc/templates/supervisor.md)
    * [crond](doc/templates/crond.md)
    * [sshd](doc/templates/sshd.md)

## 获取帮助

### 聊天室

请加入 [docker-user](http://mm.qiyi.virtual/signup_user_complete/?id=51jwf3ew1fbr9mjd141tco73rr) 聊天室来和其它 Docker 用户实时交流，以及获得开发者的实时帮助。

如果已经加入过聊天室，可以 [直接进入](http://mm.qiyi.virtual/docker)。

### 邮件列表

请订阅我们的 [邮件列表](http://dev.qiyi.com/mailman/listinfo/docker-user)，获取 Docker@iQIYI 的最新信息。

有问题可以发送邮件到：

- docker-user@dev.qiyi.com
- mesos@dev.qiyi.com

### Gitlab Issue

猛戳这里 [Dockerfiles Issues](http://gitlab.qiyi.domain/docker/dockerfiles/issues)。

按国际惯例，先搜索有没有存在的相同 issue；如果没有，再提交新的。

## 使用基础镜像

使用基础镜像非常简单，使用 `FROM` 指令即可，例如：使用 `centos6:6-iqiyi-12`
作为基础镜像。

```:Dockerfile
FROM docker-registry.qiyi.virtual/library/centos6:6-iqiyi-12
...
```

### 最新版本

| Base             | Tag              | Usage                                                                           |
| ---------------- | ---------------- | ------------------------------------------------------------------------------- |
| CentOS 6         | 6-iqiyi-12       | docker pull docker-registry.qiyi.virtual/library/centos6:6-iqiyi-12             |
| CentOS 7         | 7-iqiyi-8        | docker pull docker-registry.qiyi.virtual/library/centos7:7-iqiyi-8              |
| Ubuntu 14.04     | 14.04-iqiyi-3    | docker pull docker-registry.qiyi.virtual/library/ubuntu:14.04-iqiyi-3           |
| Ubuntu 15.10     | 15.10-iqiyi-1    | docker pull docker-registry.qiyi.virtual/library/ubuntu:15.10-iqiyi-1           |
| Debian jessie    | jessie-iqiyi-1   | docker pull docker-registry.qiyi.virtual/library/debian:jessie-iqiyi-1          |
| Golang 1.4       | 1.4-iqiyi-1      | docker pull docker-registry.qiyi.virtual/library/golang:1.4-iqiyi-1             |
| Golang 1.5       | 1.5-iqiyi-1      | docker pull docker-registry.qiyi.virtual/library/golang:1.5-iqiyi-1             |
| Golang 1.6       | 1.6-iqiyi-1      | docker pull docker-registry.qiyi.virtual/library/golang:1.6-iqiyi-1             |
| QAE minimal      | 0.5              | docker pull docker-registry.qiyi.virtual/library/qae-minimal:0.5                |
| QAE minimal      | 0.5-centos6      | docker pull docker-registry.qiyi.virtual/library/qae-minimal:0.5-centos6        |
| QAE allinone     | 0.5              | docker pull docker-registry.qiyi.virtual/library/qae-allinone:0.5               |
| QAE allinone     | 0.5-centos6      | docker pull docker-registry.qiyi.virtual/library/qae-allinone:0.5-centos6       |
| QAE tomcat       | 7-jre7-centos6-v2| docker pull docker-registry.qiyi.virtual/library/qae-tomcat:7-jre7-centos6-v2   |
| Gitlab CI Runner | centos-6-2       | docker pull docker-registry.qiyi.virtual/library/gitlab-runner:centos-6-2       |
| Gitlab CI Runner | centos-7-2       | docker pull docker-registry.qiyi.virtual/library/gitlab-runner:centos-7-2       |
| Gitlab CI Runner | ubuntu-14.04.3-1 | docker pull docker-registry.qiyi.virtual/library/gitlab-runner:ubuntu-14.04.3-1 |
| Gitlab CI Runner | ubuntu-15.10-1   | docker pull docker-registry.qiyi.virtual/library/gitlab-runner:ubuntu-15.10-1   |
| Gitlab CI Runner | golang-1.4-1     | docker pull docker-registry.qiyi.virtual/library/gitlab-runner:golang-1.4-1     |
| Gitlab CI Runner | golang-1.5-1     | docker pull docker-registry.qiyi.virtual/library/gitlab-runner:golang-1.5-1     |

## 使用模板

使用模板前，首先下载模板到项目代码中，例如：下面是你的代码目录结构

```bash
$ tree
.
├── Dockerfile
├── src
└── test
```

将模板下载到和 Dockerfile 同一目录，如下：

```bash
$ git clone ssh://git@scm.qiyi.virtual:10022/docker/dockerfiles.git
/tmp/dockerfiles
$ cp -r /tmp/dockerfiles/docker-snippets .
$ tree
.
├── Dockerfile
├── docker-snippets
│   ├── crond
│   │   ├── Dockerfile
│   │   └── resources
│   │       ├── crond.ini
│   │       └── example.cron
...
├── src
└── test
```

推荐将 docker-snippets 目录加入到你的代码管理系统中，方便跟踪模板的任何更新。
