#!/bin/bash

if [ "x$1" = "x" ]; then
    echo "Usage: $(basename $0) <directory>"
    exit 1
fi

REGISTRY_URL=docker-registry.qiyi.virtual
REPO_NAME=library/gitlab-runner
VERSION=ubuntu-$(cat $1/VERSION)

docker build -t $REGISTRY_URL/$REPO_NAME:$VERSION $1
