#!/bin/bash

function configure_log_dir()
{
    QAE_LOG_DIR=/qae/log
    # if user has changed the default qae log directory
    if [ -n "$MARATHON_INJECT_VOLUME_LOG" ]; then
        QAE_LOG_DIR=$MARATHON_INJECT_VOLUME_LOG
    fi

    echo "stdout_logfile=$QAE_LOG_DIR/catalina.log ; configure stdout" >> /etc/supervisord.d/tomcat.ini
}

configure_log_dir

exec /usr/bin/supervisord -c /etc/supervisord.conf
