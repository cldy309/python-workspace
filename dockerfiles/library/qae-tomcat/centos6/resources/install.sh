#!/bin/bash

# exit if error happened
set -e

function install_all()
{
    # install Oracle JDK
    rpm -ivh jdk-${JRE_VERSION}.x86_64.rpm
    # /usr/java/default is a link which links to the default java installed,
    # that's jdk which installed here.
    JAVA_HOME=/usr/java/default

    # install apr dependencies
    yum install -y tar gcc apr-devel openssl-devel unzip patch bc wget

    # install Tomcat
    tar xzf apache-tomcat-${TOMCAT_VERSION}.tar.gz -C /opt
    CATALINA_HOME=/opt/apache-tomcat-${TOMCAT_VERSION}
    ln -sf $CATALINA_HOME /opt/tomcat

    # install tomcat apr module
    pushd /opt/tomcat/bin
    tar xzf tomcat-native.tar.gz
    pushd tomcat-native-1.1.33-src/jni/native
    ./configure --with-apr=/usr/bin/apr-1-config \
        --with-java-home=$JAVA_HOME \
        --with-ssl=yes \
        --prefix=$CATALINA_HOME
    make && make install
    popd
    popd

    rpm -e jdk
    rpm -ivh jre-${JRE_VERSION}.x86_64.rpm
    yum remove -y gcc tar apr-devel openssl-devel && yum clean all
    mv -f start.sh /usr/bin/
    rm -rf /tmp/resources
}

install_all
