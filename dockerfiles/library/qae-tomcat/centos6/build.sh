#!/bin/bash
#

set -e

REGISTRY_URL=docker-registry.qiyi.virtual
REPO_NAME=library/qae-tomcat
VERSION=$(cat VERSION)

docker build -t $REGISTRY_URL/$REPO_NAME:$VERSION .
