#!/bin/bash

REGISTRY_URL=docker-registry.qiyi.virtual
REPO_NAME=library/debian
VERSION=$(cat VERSION)

docker build -t $REGISTRY_URL/$REPO_NAME:$VERSION .
