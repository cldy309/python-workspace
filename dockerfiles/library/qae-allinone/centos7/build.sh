#!/bin/bash
#

set -e

cp -r ../../../docker-snippets .

REGISTRY_URL=docker-registry.qiyi.virtual
REPO_NAME=library/qae-allinone
VERSION=$(cat VERSION)

docker build -t $REGISTRY_URL/$REPO_NAME:$VERSION .
