#coding=utf-8

import os
import sys

def dic_filter(path='filter_dic.txt'):
	if os.path.exists(path) == False:
		return

	b_sensitive = False
	type_in_string = raw_input('>').decode(sys.stdin.encoding)
	with open(path) as f:
		text = f.read().decode('utf-8')
		# text = unicode(text,'utf-8')

	for word in text.split('\n'):
		if word in type_in_string:
			b_sensitive = True

	if b_sensitive:
		print 'is sensitive'
	else:
		print 'all be fine'

if __name__ == '__main__':
	while True:
		dic_filter()
