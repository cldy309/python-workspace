#coding=utf8

import os

def walk_dir(path):
	if os.path.exists(path) == False:
		return

	for path , dirs , files in os.walk(path):
		print path , dirs , files
		for filename in files:
			if filename.lower().endswith('jpg'):
				fullpath = os.path.join(path , filename)
				print fullpath


if __name__ == '__main__':
	walk_dir('./')
