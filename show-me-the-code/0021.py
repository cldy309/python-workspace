#coding=utf-8

import os 
import sys
from hashlib import sha256
from hmac import HMAC

def encrypt_password(password , salt = None):
	"""
	hash password on the fly.
	"""
	if salt is None:
		# salt is 8 bytes len
		salt = os.urandom(8) 

	print 'salt' , salt

	assert len(salt) == 8
	assert isinstance(salt , str)

	if isinstance(password , unicode):
		password = password.encode('utf-8')

	assert isinstance(password , str)

	result = password

	for i in range(10):
		result = HMAC(result , salt , sha256).digest()

	return salt + result


def validate_password(hashed , input_password):
	return hashed == encrypt_password(input_password , salt = hashed[:8])


if __name__ == '__main__':
	password = raw_input('>').decode(sys.stdin.encoding)
	password_saved = encrypt_password(password)

	password_verify = raw_input('(type in again)>').decode(sys.stdin.encoding)

	assert validate_password(password_saved , password_verify)