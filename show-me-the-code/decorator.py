
import logging
def use_logging(func):
	def wrapper(*args , **kvargs):
		logging.warn("%s is running %s" %(func.__name__ , args))
		return func(*args)

	return wrapper

@use_logging
def foo(log):
	print 'foo is called'


foo('test')