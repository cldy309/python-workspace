#-*- coding:utf-8 -*-
#0000

from PIL import Image, ImageDraw, ImageFont

def addNum(img):
	draw = ImageDraw.Draw(img)
	font = ImageFont.truetype('Futura.ttf',size = 40)
	color = '#ff0000'
	w , h = img.size

	draw.text((w - 60 , 0),'99',font=font , fill=color)
	img.save('result.png' , 'png')

if __name__ == '__main__':
	image = Image.open('test.png')
	addNum(image)