#coding=utf8

import os
import re

def walk_dir(path , endwith='.txt'):
	file_path = []
	if os.path.exists(path) == False:
		return file_path

	for path , dirs , file_list in os.walk(path):
		for file_name in file_list:
			if file_name.lower().endswith(endwith):
				file_path.append(os.path.join(path , file_name))

	return file_path


def find_key_word(filepath):
	word_dic = {}

	file_name = os.path.basename(filepath)
	with open(filepath) as f:
		text = f.read()
		word_list = re.findall(r'[A-Za-z]+', text.lower())

		if len(word_list) > 0:
			for w in word_list:
				if w in word_dic:
					word_dic[w] += 1
				else:
					word_dic[w] = 1

			sorted_word_list = sorted(word_dic.items() , key = (lambda k: k[1]))
			print u"在%s文件中,%s为关键字，共出现了%s次" %(file_name , sorted_word_list[-1][0] , sorted_word_list[-1][1])


if __name__ == '__main__':
	for file_path in walk_dir(os.getcwd()):
		find_key_word(file_path)