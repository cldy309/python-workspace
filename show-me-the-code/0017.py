# coding=utf-8

"""
第 0017 题： 将 第 0014 题中的 student.xls 文件中的内容写到 student.xml 文件中，如
下所示：
<?xml version="1.0" encoding="UTF-8"?>
<root>
<students>
<!--
    学生信息表
    "id" : [名字, 数学, 语文, 英文]
-->
{
    "1" : ["张三", 150, 120, 100],
    "2" : ["李四", 90, 99, 95],
    "3" : ["王五", 60, 66, 68]
}
</students>
</root>
"""
import xlrd 
import json
from lxml import etree

def read_excel(path , *sheetname):
	book = xlrd.open_workbook(path)
	sheetnames = list(sheetname)
	if len(sheetnames) == 0:
		sheetnames = book.sheet_names()

	content = {}

	for sheet in sheetnames:
		exl_sheet = book.sheet_by_name(sheet)
		data = []
		for row in range(exl_sheet.nrows):
			data.append(exl_sheet.row_values(row))

		content[sheet] = json.dumps(data , encoding='utf-8')
	return content
	# return json.dumps(content , encoding='utf-8')

def save_to_xml(content , path):
	root = etree.Element('root')
	for k , v in content.items():
		node = etree.SubElement(root , k)
		# or etree.Comment(k.decode('utf-8'))
		node.append(etree.Comment(k))
		node.text = v
	xml = etree.ElementTree(root)
	xml.write(path , pretty_print=True , xml_declaration=True , encoding='utf-8')

if __name__ == '__main__':
	content = read_excel('student.xls')
	save_to_xml(content , 'student.xml')



