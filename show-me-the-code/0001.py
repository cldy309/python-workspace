import uuid
import pymysql

def createCode(num , lenght ,filename='activation.txt'):
	result = []
	while True:
		uid = uuid.uuid1()
		tmp = str(uid).replace('-','')[:lenght]
		print tmp

		if tmp not in result:
			result.append(tmp)
		if len(result) == num:
			break

	saveToSql(result)

	f = open(filename , 'w')

	for res in result:
		f.write(res + '\n')
	f.close()

def saveToSql(numList):
	try:
		conn = pymysql.connect(host='localhost',user='root',passwd='openwrt',port=3306,charset='utf8')
		cur = conn.cursor()

		database = 'create database if not exists python_activecode_db'
		cur.execute(database)

		conn.select_db('python_activecode_db')
		table = 'create table if not exists active_codes(active_code varchar(32))'

		cur.execute(table)

		insert = 'insert into active_codes values(%s)'
		# for num in numList:
		# 	cur.execute(insert , num)
		cur.executemany(insert , numList)

		conn.commit()
	except pymysql.Error, e:
		print 'Mysql error %d:%s' %(e.args[0],e.args[1])
	finally:
		cur.close()
		conn.close()


if __name__ == '__main__':
	createCode(200 , 20)