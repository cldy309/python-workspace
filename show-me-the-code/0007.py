#coding=utf8
"""
这是注释吗
"""

'''
这是注释
'''
import os
import random

def generate_random_letter():
	return random.choice(string.ascii_letters)

def walk_dir(path , endwith='.py'):
	file_path = []

	if os.path.exists(path) == False:
		return file_path

	for path , dirs , file_list in os.walk(path):
		for file_name in file_list:
			if file_name.lower().endswith(endwith):
				file_path.append(os.path.join(path,file_name))

	return file_path


def line_count(path):

	file_name = os.path.basename(path)
	total_line = 0
	empty_line = 0
	comment_line = 0
	in_comment = False

	with open(path) as f:
		for line in f.read().split('\n'):
			total_line += 1
			if line.strip().startswith(r'"""') and not in_comment:
				comment_line += 1
				in_comment = True
				continue

			if line.strip().startswith(r'"""'):
				in_comment = False
				comment_line += 1

			if line.strip().startswith(r'//') or line.strip().startswith('#') or in_comment:
				comment_line += 1

			if len(line) == 0:
				empty_line += 1

	print u'在%s中，共有%s行代码,其中有%s空行，有%s注释' %(file_name , total_line , empty_line , comment_line)

if __name__ == '__main__':
	for f in walk_dir(os.getcwd()):
		line_count(f)
