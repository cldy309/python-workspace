#coding=utf-8

import os
from bs4 import BeautifulSoup
import urllib
from urlparse import urlsplit

pics_dir = './pics'

def fetch_pics(url):
	content = urllib.urlopen(url)
	bs = BeautifulSoup(content , 'lxml')
	for urls in bs.find_all('img' , {"class":"BDE_Image"}):
		download_pic(urls['src'])


def download_pic(url):
	img_content = urllib.urlopen(url).read()
	print urlsplit(url)
	file_name = os.path.basename(urlsplit(url)[2])
	with open(pics_dir + os.sep + file_name , 'wb') as f:
		f.write(img_content)

if __name__ == '__main__':
	if not os.path.exists(pics_dir):
		os.mkdir(pics_dir)

	fetch_pics('http://tieba.baidu.com/p/2166231880')