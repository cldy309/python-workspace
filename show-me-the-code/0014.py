#coding=utf-8

import os
import json
import xlwt
import sys

def read_text(path):
	if not os.path.exists(path):
		return

	with open(path, 'r') as f:
		text = f.read().decode('utf-8')
		print text
		text_json = json.loads(text)

	return text_json


def save_excel_dict(content , sheetname ,des):

	wb = xlwt.Workbook()
	ws = wb.add_sheet(sheetname,cell_overwrite_ok=True)
	row = 0
	col = 0

	for k , v in sorted(content.items(),key = lambda d: d[0]):
		ws.write(row , col , k)
		for word in v:
			col += 1
			ws.write(row , col , word)

		row += 1
		col = 0

	wb.save(des)
def save_excel_list(content , sheetname , des):
	wb = xlwt.Workbook()
	ws = wb.add_sheet(sheetname , cell_overwrite_ok = True)
	row = 0
	col = 0

	for l in content:
		for word in l:
			ws.write(row , col , word)
			col += 1

		row += 1
		col = 0
	wb.save(des)

def save_excel(content , sheetname , des):
	dd = {
		type([]) : save_excel_list,
		type({}) : save_excel_dict
	}
	if type(content) in dd:
		dd[type(content)](content , sheetname , des)

if __name__ == '__main__':
	if len(sys.argv) == 1:
		print 'Usage:python filepath'
		os._exit(-1)

	filepath = sys.argv[1]
	filename = os.path.basename(filepath)	
	name = filename[0:filename.rindex('.')]

	content = read_text(os.path.join(os.path.split(__file__)[0] , filename))
	save_excel(content , name , os.path.join(os.path.split(__file__)[0] , name+'.xls'))