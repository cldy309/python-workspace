import os
import string
from itertools import groupby
import time as t

def gb(name):
	ch = name[0].lower()
	if ch in string.lowercase[:26]:
		return ch
	else:
		return '#'

sourceDir = '/Users/clong/Downloads/Kindle Library (MOBI)/'

bookDirs = os.listdir(sourceDir)

targetDir = './Kindle-Library-MOBI'

if not os.path.exists(targetDir):
	os.mkdir(targetDir)
	print 'Successfully created directory' , targetDir

print t.time() , 'Start compressing...'

for cat , source in groupby(bookDirs , gb):
	target = targetDir + os.sep + cat + '.zip'
	# ll = []
	ll = map(lambda name : '"'+sourceDir+name+'"' , source)
	# for name in source:
	# 	ll.append('"'+sourceDir+name+'"')
	zip_command = 'zip -qr {0} {1}'.format(target , ' '.join(ll))
	# print zip_command
	startTime = t.time()
	print 'comppressing {0} ...'.format(target)
	if os.system(zip_command) == 0:
		print 'Successfully zip to' , target ,
	else :
		print 'zip failed!' , target ,

	print t.time() - startTime